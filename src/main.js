import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入vant所有组件
import Vant from 'vant'
import 'vant/lib/index.css'

// 动态设置ren值
import 'amfe-flexible'

Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
